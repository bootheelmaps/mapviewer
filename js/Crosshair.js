/**
 * @requires OpenLayers/Control.js
 */

/**
 * Class: OpenLayers.Control.Crosshair
 * The crosshair control adds a crosshair to the very center of the map
 * for easily zooming in to the desired point.
 *
 * Inherits from:
 *  - <OpenLayers.Control>
 */
OpenLayers.Control.Crosshair = 
  OpenLayers.Class(OpenLayers.Control, {
    
    /**
     * Constructor: OpenLayers.Control.Crosshair 
     * 
     * Parameters:
     * options - {Object} Options for control.
     */
    initialize: function(options) {
        OpenLayers.Control.prototype.initialize.apply(this, arguments);
    },

    /** 
     * Method: destroy
     * Destroy control.
     */
    destroy: function() {
        this.map.events.un({
            "removelayer": this.updateCrosshair,
            "addlayer": this.updateCrosshair,
            "changelayer": this.updateCrosshair,
            "changebaselayer": this.updateCrosshair,
            scope: this
        });
        
        OpenLayers.Control.prototype.destroy.apply(this, arguments);
    },    
    
    /**
     * Method: draw
     * Initialize control.
     * 
     * Returns: 
     * {DOMElement} A reference to the DIV DOMElement containing the control
     */    
    draw: function() {
        OpenLayers.Control.prototype.draw.apply(this, arguments);
        
        this.map.events.on({
            'changebaselayer': this.updateCrosshair,
            'changelayer': this.updateCrosshair,
            'addlayer': this.updateCrosshair,
            'removelayer': this.updateCrosshair,
            scope: this
        });
        this.updateCrosshair();
        
        return this.div;    
    },

    /**
     * Method: updateCrosshair
     * Update Crosshair.
     */
    updateCrosshair: function() {
        var attributions = [];
        if (this.map && this.map.layers) {
            this.div.innerHTML = "<div class='CHN'></div><div class='CHS'></div><div class='CHE'></div><div class='CHW'></div>";
        }
    },

    CLASS_NAME: "OpenLayers.Control.Crosshair"
});