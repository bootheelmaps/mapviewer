/**
 * @requires OpenLayers/Control.js
 */

/**
 * Class: OpenLayers.Control.Title
 * Inherits from:
 *  - <OpenLayers.Control>
 */
OpenLayers.Control.Title = OpenLayers.Class(OpenLayers.Control, {

    titleText: "Bootheel Maps",
    
    /**
     * APIProperty: autoActivate
     * {Boolean} Activate the control when it is added to a map.  Default is
     *     true.
     */
    autoActivate: true,


    /**
     * Method: destroy
     */
     destroy: function() {
         OpenLayers.Control.prototype.destroy.apply(this, arguments);
     },

    /**
     * APIMethod: activate
     */
    activate: function() {
        if (OpenLayers.Control.prototype.activate.apply(this, arguments)) {
          this.element.innerHTML = "<strong>" + this.titleText + "</strong>";
          return true;
        } else {
          return false;
        }
    },
    
    /**
     * Method: draw
     * {DOMElement}
     */    
    draw: function() {
        OpenLayers.Control.prototype.draw.apply(this, arguments);

        if (!this.element) {
            this.div.left = "";
            this.div.top = "";
            this.element = this.div;
        }
        
        return this.div;
    },
   
    CLASS_NAME: "OpenLayers.Control.Title"
});
