Bootheel Maps interactive MapViewer and MobileViewer
Version: v20121211  

The latest version of this map is available as a free download from
www.BootheelMaps.com


USAGE: Extract the contents of this archive file to any location on your
hard drive or SD card while maintaining the structure of the subdirectories.

OPEN MapViewer.html or MobileViewer.html with any web browser.

MapViewer.html is designed for use on a computer or laptop with a keyboard
and mouse or mouse-like pointing device.

MobileViewer.html is designed for use on a tablet or phone that uses a 
touch screen interface.  This version is also intended to work well 
with a relatively small screen.

NOTE that no network connection is required to use these interactive map
viewers.  All required data is provided in this archive file.  Thus, the
interactive map viewers may be used on computers or mobile devices in areas 
with no internet or phone service.  


For printed maps and a gazetteer (annotated index), download the maps as 
PDF files from www.BootheelMaps.com.

Corrections, additions, and comments are welcome.  In any correspodence,
please include the title and the version from the map that you are 
using, as well as whether you are using a Special Edition.  The version 
number for any map from a PDF file can be found near the southwest corner
of the map and has the form vYYYYMMDD where YYYY is the year, MM is the
month, and DD the data that the map was last updated.  If the map you are 
using is a Special Edition, an indication of this will immediately follow
the version number.  If you are using the interactive MapViewer or 
MobileViewer, the version number will be found in the README.txt file
distributed with it.  Please send any corrections, comments, or questions
to Info@BootheelMaps.com.  

Any information (additions, corrections, comments, etc.) submitted will be
assumed to have been released ot the public domain without any restrictions.
